# -*- coding: utf-8 -*-
import pyexcel_xlsx as pe
import requests


class VerifyLink(object):
    ReadDirectory = 'read/'
    WriteDirectory = 'out/'

    @classmethod
    def verify_links(cls, url):
        try:
            r = requests.get(url)
        except Exception as e:
            print(e.message)
            return 'error'
        else:
            return r.status_code

    def __init__(self):
        self.data = None

    def read_directory(self):

        return self.ReadDirectory + 'test.xlsx'

    def write_directory(self):

        return self.WriteDirectory + 'test.o.xlsx'

    def read_data(self):
        directory = self.read_directory()
        self.data = pe.get_data(directory)

    def start_verifying(self):
        target_data = self.data['Sheet1']
        index = 1
        while index < len(target_data):
            this_row = target_data[index]
            result = self.verify_links(this_row[1])
            print("%s %s" % (index, result))
            if result == 200:
                this_row.append('vaild')
            elif result == 'error':
                this_row.append('error')
            else:
                this_row.append('invaild')

            index += 1

    def write_data(self):
        directory = self.write_directory()
        pe.save_data(directory, self.data)


if __name__ == '__main__':
    import time
    start = time.time()
    vfl = VerifyLink()
    vfl.read_data()
    vfl.start_verifying()
    vfl.write_data()
    end = time.time()
    print(end - start)
