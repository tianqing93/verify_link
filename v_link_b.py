# -*- coding: utf-8 -*-
import pyexcel_xlsx as pe
import requests


class VerifyLink(object):
    ReadDirectory = 'read/'
    WriteDirectory = 'out/'

    def __init__(self):
        self.data = None
        self.links = None

    def read_directory(self):

        return self.ReadDirectory + 'test.xlsx'

    def write_directory(self):

        return self.WriteDirectory + 'test.o.xlsx'

    def read_data(self):
        directory = self.read_directory()
        self.data = pe.get_data(directory)

    def write_data(self):
        directory = self.write_directory()
        pe.save_data(directory, self.data)

    def verify_links(self):
        links = self.data['Sheet1']
        for index, link in enumerate(links):
            if index == 0:
                continue
            try:
                r = requests.get(link[1])
            except Exception as e:
                print(e.message)
                link.append('error')
            else:
                print("%s %s" % (index, r.status_code))
                if r.status_code == 200:
                    link.append('valid')
                else:
                    link.append('invalid')

if __name__ == "__main__":
    import time
    start = time.time()
    vfl = VerifyLink()
    vfl.read_data()
    vfl.verify_links()
    vfl.write_data()
    end = time.time()
    print(end - start)